<?php
$PageTitle = "Register";

$username = "";
$password = "";
$passwordRepeated = "";
$email = "";
$firstName = "";
$lastName = "";
$dateOfBirth = "";
$postCode = "";

$usernameError = "";
$emailError = "";
$passwordError = "";
$passwordRepeatedError = "";
$firstNameError = "";
$lastNameError = "";
$dateOfBirthError = "";
$postCodeError = "";
$submitError = "";



include_once "php/functions.php";



/*========== Content ==========*/

include "php/header.php";

/*========== POST Processing ==========*/

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    /* Get posted data */
    $username = filterInput($_POST['username']);
    $email = filterInput($_POST['email']);
    $password = filterInput($_POST['password']);
    $passwordRepeated = filterInput($_POST['passwordRepeated']);
    $firstName = filterInput($_POST['firstName']);
    $lastName = filterInput($_POST['lastName']);
    $dateOfBirth = filterInput($_POST['dateOfBirth']);
    $postCode = filterInput($_POST['postCode']);

    /* Server sided input validation */
    $validInputs = true;
    include "php/register/validation.php";

    if ($validInputs) {
        /* Register the user, log them in and redirect to index.php */
        processRegistration($username, $password, $email, $firstName, $lastName, $dateOfBirth, $postCode);
        header("Location: index.php");
        exit();
    }
}

?>
  <script type="text/javascript" src="js/register.js"></script>

  <h1>Register</h1>

<?php

include "php/register/form.php";

include "php/footer.php";



/*========== Functions ==========*/

// Checks if the e-mail already exists in the database.
function checkUniqueEmail($email)
{
    include 'php/database.php';
    try {
        $stmt = $pdo->prepare('SELECT * FROM members WHERE Email=:email');
        $stmt->bindValue(':email', $email);
        $stmt->execute();
        $count = $stmt->rowcount();
    } catch (PDOException $e) {
        echo $e->getMessage();
        return;
    }
    return $count == 0;
}

// Checks if the username already exists in the database.$_COOKIE
function checkUniqueUsername($username)
{
    include 'php/database.php';
    try {
        $stmt = $pdo->prepare('SELECT * FROM members WHERE Username=:username');
        $stmt->bindValue(':username', $username);
        $stmt->execute();
        $count = $stmt->rowcount();
    } catch (PDOException $e) {
        echo $e->getMessage();
        return;
    }
    return $count == 0;
}

// Processes the supplied registration details, returning true upon successful login.
function processRegistration($username, $password, $email, $firstName, $lastName, $dateOfBirth, $postCode)
{
    include 'php/database.php';

    /* Prepare variables to be inserted into database */
    $password_hash = password_hash($password, PASSWORD_DEFAULT);
    $dateOfBirth_formatted = date('Y-m-d', strtotime(str_replace('/', '-', $dateOfBirth)));
    
    /* Insert new member into database */
    try {
        $stmt = $pdo->prepare('INSERT INTO members (Username, Password, Email, FirstName, LastName, PostCode, DateOfBirth)'.
        'VALUES(:username, :password, :email, :firstName, :lastName, :postCode, :dateOfBirth)');
        $stmt->bindValue(':username', $username);
        $stmt->bindValue(':password', $password_hash);
        $stmt->bindValue(':email', $email);
        $stmt->bindValue(':firstName', $firstName);
        $stmt->bindValue(':lastName', $lastName);
        $stmt->bindValue(':postCode', $postCode);
        $stmt->bindValue(':dateOfBirth', $dateOfBirth_formatted);
        $stmt->execute();
    } catch (PDOException $e) {
        echo $e->getMessage();
        return;
    }

    /* Log the user in immediately after registration */
    try {
        $stmt = $pdo->prepare('SELECT * FROM members WHERE Username=:username');
        $stmt->bindValue(':username', $username);
        $stmt->execute();
    } catch (PDOException $e) {
        echo $e->getMessage();
        return;
    }

    /* Set user's session variables */
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    $_SESSION['UserID'] = $result['UserID'];
    $_SESSION['Username'] = $result['Username'];
    $_SESSION['FirstName'] = $result['FirstName'];
    $_SESSION['LastName'] = $result['LastName'];

    return true;
}
