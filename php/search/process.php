<?php

unset($_SESSION['parkName']);
unset($_SESSION['suburb']);
unset($_SESSION['distance']);
unset($_SESSION['rating']);

/* Search by Name */
if (isset($_POST['parkName'])) {
    $name = filterInput($_POST['parkName']);

    $validInputs = true;
    if (empty($name)) {
        $nameError = "Enter a park name to search.";
        $validInputs = false;
    }

    if ($validInputs) {
        $_SESSION['parkName'] = $name;
        header("Location: results.php");
        exit();
    }
}

/* Search by Suburb */
if (isset($_POST['suburb'])) {
    $suburb = filterInput($_POST['suburb']);

    $validInputs = true;
    if (empty($suburb)) {
        $suburbError = "Select a suburb.";
        $validInputs = false;
    }

    if ($validInputs) {
        $_SESSION['suburb'] = $suburb;
        header("Location: results.php");
        exit();
    }
}

/* Search by Distance */
if (isset($_POST['distance'])) {
    $distance = filterInput($_POST['distance']);

    $validInputs = true;
    if (empty($distance)) {
        $distanceError = "Enter a distance in kilometers.";
        $validInputs = false;
    } elseif (!is_numeric($distance) || $distance <= 0) {
        $distanceError = "Distance must be a positive number.";
        $validInputs = false;
    }

    if ($validInputs) {
        $_SESSION['distance'] = $distance;
        header("Location: results.php");
        exit();
    }
}

/* Search by Rating */
if (isset($_POST['rating'])) {
    $rating = filterInput($_POST['rating']);

    $validInputs = true;
    if (empty($rating)) {
        $ratingError = "Select a rating.";
        $validInputs = false;
    }

    if ($validInputs) {
        $_SESSION['rating'] = $rating;
        header("Location: results.php");
        exit();
    }
}