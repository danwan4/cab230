// Validates the login form inputs, returning true if valid
function validateLoginForm() {
	var validUsername = checkUsername();
	var validPassword = checkPassword();

	if (validUsername && validPassword) {
		return true;
	} else {
		// Hide old submit error in case user submitted again without changing inputs
		hideElement("submitError");
		return false;
	}
}

// Hides an element of a given id
function hideElement(id) {
	document.getElementById(id).style.visibility = "hidden";
}

// Hides a given id, and the submit error
function hideError(id) {
	hideElement(id);
	hideElement("submitError");
}

// Checks if username input is valid
function checkUsername() {
	var field = document.forms["login"]["username"].value;

	// Check if input is empty
	if (field === "") {
		document.getElementById("usernameInvalid").innerHTML = "This is a required field.";
		document.getElementById("usernameInvalid").style.visibility = "visible";
		return false;
	}
	return true;
}

// Checks if password input is valid
function checkPassword() {
	var field = document.forms["login"]["password"].value;

	// Check if input is empty
	if (field === "") {
		document.getElementById("passwordInvalid").innerHTML = "This is a required field.";
		document.getElementById("passwordInvalid").style.visibility = "visible";
		return false;
	}
	return true;
}