<p>Please enter your username and password.</p>

<form name="login" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">

  <!-- Username -->
  <div class="formInput">
    <div class="formInputTitle">Username</div>
    <input type="text" class="formInputField" name="username" onkeypress="hideError('usernameInvalid');" value=<?=$username?>>
    <div id="usernameInvalid" class="formError"><?=$usernameError?></div>
  </div>
  <!-- Password -->
  <div class="formInput">
    <div class="formInputTitle">Password</div>
    <input type="password" class="formInputField" name="password" onkeypress="hideError('passwordInvalid');">
    <div id="passwordInvalid" class="formError"><?=$passwordError?></div>
  </div>

  <!-- Submit Button -->
  <input type="submit" class="formSubmitButton" value="Login" onclick="return validateLoginForm();">
  <div id="submitError" class="formError"><?=$submitError?></div>
</form>
