// Hides an element of a given id
function hideElement(id) {
  document.getElementById(id).style.visibility = "hidden";
}

// Checks if park name search input is valid
function validateNameForm() {
  var field = document.forms["nameSearch"]["parkName"].value;
  // Check if input is empty
  if (field === "") {
    document.getElementById("nameInvalid").innerHTML = "This is a required field.";
    document.getElementById("nameInvalid").style.visibility = "visible";
    return false;
  }
  return true;
}

// Checks if suburb search input is valid
function validateSuburbForm() {
  var field = document.forms["suburbSearch"]["suburb"].value;

  // Check if input is empty
  if (field === "") {
    document.getElementById("suburbInvalid").innerHTML = "You must select a suburb.";
    document.getElementById("suburbInvalid").style.visibility = "visible";
    return false;
  }
  return true;
}

// Checks if distance search input is valid
function validateDistanceForm() {
  var field = document.forms["distanceSearch"]["distance"].value;

  // Check if input is empty
  if (field === "") {
    document.getElementById("distanceInvalid").innerHTML = "This is a required field.";
    document.getElementById("distanceInvalid").style.visibility = "visible";
    return false;
  // Check if not a number or negative value
  } else if (isNaN(field) || field <= 0) {
    document.getElementById("distanceInvalid").innerHTML = "Distance must be a positive number."
    document.getElementById("distanceInvalid").style.visibility = "visible";
    return false;
  }
  return true;
}

// Checks if rating search input is valid
function validateRatingForm() {
  var field = document.forms["ratingSearch"]["rating"].value;

  // Check if input is empty
  if (field === "") {
    document.getElementById("ratingInvalid").innerHTML = "You must select a minimum rating.";
    document.getElementById("ratingInvalid").style.visibility = "visible";
    return false;
  }
  return true;
}