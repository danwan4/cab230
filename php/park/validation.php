<?php

// Review Title
if (empty($reviewTitle)) {
    $reviewTitleError = "This is a required field.";
    $validInputs = false;
}

// Rating
if (empty($rating)) {
    $ratingError = "This is a required field.";
    $validInputs = false;
} elseif ($rating < 1 || $rating > 5) {
    $rating = "The rating you submitted is out of range.";
    $validInputs = false;
}

// Review Text
if (empty($reviewText)) {
    $reviewtextError = "This is a required field.";
    $validInputs = false;
}
