<?php
$PageTitle = "Login";

$username = "";

$usernameError = "";
$passwordError = "";
$submitError = "";



include_once "php/functions.php";



/*========== Content ==========*/

include_once "php/header.php";

/*========== POST Processing ==========*/

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    /* Get posted data */
    $username = filterInput($_POST['username']);
    $password = filterInput($_POST['password']);

    /* Server sided input validation */
    $validInputs = true;
    include "php/login/validation.php";

    if ($validInputs) {
        if (processLogin($username, $password)) {
            /* Redirect to index.php */
            header("Location: index.php");
        } else {
            /* Show form with error messages */
            $submitError = "Incorrect username or password. Please try again.";
        }
    }
}

?>
  <script type="text/javascript" src="js/login.js"></script>

  <h1>Login</h1>

<?php

include "php/login/form.php";

include "php/footer.php";



/*========== Functions ==========*/

// Processes the supplied login details, returning true upon successful login.
function processLogin($username, $password)
{
    include 'php/database.php';

    /* Check for matching username in the database */
    try {
        $stmt = $pdo->prepare('SELECT * FROM members WHERE Username=:username');
        $stmt->bindValue(':username', $username);
        $stmt->execute();
        $count = $stmt->rowcount();
    } catch (PDOException $e) {
        echo $e->getMessage();
        return;
    }
    
    /* Was there a username match? */
    if ($count != 1) {
        return false;
    }

    /* There was a username match, so check if password matches */
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    $password_hash = $result['Password'];
    if (!password_verify($password, $password_hash)) {
        return false;
    }

    /* Set user's session variables */
    $_SESSION['UserID'] = $result['UserID'];
    $_SESSION['Username'] = $result['Username'];
    $_SESSION['FirstName'] = $result['FirstName'];
    $_SESSION['LastName'] = $result['LastName'];

    return true;
}
