<form name="review" id="reviewForm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) . "?park=" . $parkID;?>" method="post">

    <!-- Title and Rating in two columns at top of form -->
    <div class="formInputContainer">
        <!-- Review Title -->
        <div class="formInput">
            <div class="formInputTitle">Title</div>
            <input type="text" class="formInputField" name="reviewTitle" onkeypress="hideError('reviewTitleInvalid');" value=<?=$reviewTitle?>>
            <div id="reviewTitleInvalid" class="formError"><?=$reviewTitleError?></div>
        </div>
        <!-- Rating -->
        <div class="formInput">
            <div class="formInputTitle">Rating</div>
            <select form="reviewForm" class="formInputField" id="ratingInput" name="rating" onchange="hideError('ratingInvalid');">
                <option value="" selected disabled hidden>Select a rating...</option>
                <?php
                for ($i=1; $i <= 5; $i++) {
                    echo "<option value=\"$i\"";
                    /* Select the rating if it has been POSTed but failed server validation */
                    if ($rating == $i) {
                        echo " selected";
                    }
                    echo " class=\"review-rating\">" . str_repeat("★", $i) . "</option>";
                }?>                
            </select>
            <div id="ratingInvalid" class="formError"><?=$ratingError?></div>
        </div>
    </div>

    <!-- Review text is input using full width input text area -->
    <div class="formInput">
        <div class="formInputTitle">Your Review</div>
        <textarea form="reviewForm" class="formInputField" id="reviewTextInput" name="reviewText" onkeypress="hideError('reviewTextInvalid');"><?=$reviewText?></textarea>
        <div id="reviewTextInvalid" class="formError"><?=$reviewTextError?></div>
    </div>

    <input type="submit" class="formSubmitButton" value="Submit Review" onclick="return validateReviewForm();">
    <div id="submitError" class="formError"><?=$submitError?></div>
</form>
