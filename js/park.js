// Validates the review form inputs, returning true if valid
function validateReviewForm() {
    var validReviewTitle = checkReviewTitle();
    var validRating = checkRating();
    var validReviewText = checkReviewText();

    if (validReviewTitle && validRating && validReviewText) {
        return true;
    } else {
        // Hide old submit error in case user submitted again without changing inputs
        hideElement("submitError");
        return false;
    }
}

// Hides an element of a given id
function hideElement(id) {
    document.getElementById(id).style.visibility = "hidden";
}

// Hides a given id, and the submit error
function hideError(id) {
    hideElement(id);
    hideElement("submitError");
}

// Checks if review title input is valid
function checkReviewTitle() {
    var field = document.forms["review"]["reviewTitle"].value;

    // Check if input is empty
    if (field === "") {
        document.getElementById("reviewTitleInvalid").innerHTML = "This is a required field.";
        document.getElementById("reviewTitleInvalid").style.visibility = "visible";
        return false;
    }
    return true;
}

// Checks if rating input is valid
function checkRating() {
    var field = document.getElementById("ratingInput").value;

    // Check if input is empty
    if (field === "") {
        document.getElementById("ratingInvalid").innerHTML = "Please select a rating.";
        document.getElementById("ratingInvalid").style.visibility = "visible";
        return false;
    }
    return true;
}

// Checks if review input is valid
function checkReviewText() {
    var field = document.getElementById("reviewTextInput").value;

    // Check if input is empty
    if (field === "") {
        document.getElementById("reviewTextInvalid").innerHTML = "This is a required field.";
        document.getElementById("reviewTextInvalid").style.visibility = "visible";
        return false;
    }
    return true;
}