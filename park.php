<?php
if (!isset($_GET['park'])) {
    /* User shouldn't be here */
    header("Location: index.php");
    exit();
}

$parkID = $_GET['park'];
$parkInfo = getParkInfo($_GET['park']);
if ($parkInfo != false) {
    $PageTitle = $parkInfo['Name'];
} else {
    $PageTitle = "Invalid Park";
}

$reviewTitle = "";
$reviewTitleError = "";
$rating = "";
$ratingError = "";
$reviewText = "";
$reviewTextError = "";
$submitError = "";



include_once "php/functions.php";



/*========== Content ==========*/

include "php/header.php";

/*========== POST Processing ==========*/

if ($_SERVER["REQUEST_METHOD"] == "POST" && isLoggedIn() && !hasAlreadyReviewed($parkID, $_SESSION['UserID'])) {
    /* Get posted data */
    $reviewTitle = filterInput($_POST['reviewTitle']);
    $rating = filterInput($_POST['rating']);
    $reviewText = filterInput($_POST['reviewText']);

      /* Server sided input validation */
    $validInputs = true;
    include "php/park/validation.php";

    if ($validInputs) {
        /* Submit the review to the database */
        submitReview($parkID, $_SESSION['UserID'], $reviewTitle, $rating, $reviewText);
    }
}

?>
  <script type="text/javascript" src="js/park.js"></script>

<!-- Park Information -->

<?php
/* Check in case user inputted invalid park ID */

if ($parkInfo != false) {
    echo "<h1>" . $parkInfo['Name'] . "</h1>";
    echo "<p>" . $parkInfo['Street'] . ", " . $parkInfo['Suburb'] . "</p>";
} else {
    /* Users shouldn't be here */
    echo "<h1>This park doesn't exist!</h1>";
    include "php/footer.php";
    exit();
}

createMap($parkInfo['Latitude'], $parkInfo['Longitude']);
?>


<!-- Reviews -->

<h2>User Reviews</h2>

<?php
$reviews = getReviews($parkID);
if ($reviews == false) {
    echo "<p>There are no reviews for this park yet.</p>";
} else {
    echo "<div>";
    foreach ($reviews as $review) {
        echo "<div class=\"review\">";

        echo "<div class=\"reviewer-info\">";
        echo "<div class=\"reviewer-name\">" . $review['Username'] . "</div>";
        echo "<div class=\"review-stats\">" . getUserReviewCountString($review['UserID']) . "</div>";
        echo "</div>";

        echo "<div class=\"review-content\">";

        echo "<div class=\"review-heading\">";
        echo "<div class=\"review-rating\">";
        /* Show the star rating  of the review */
        for ($i=0; $i < $review['Rating']; $i++) {
            echo "★";
        }
        echo "</div>";
        echo "<h3 class=\"review-title\">" . $review['Title'] . "</h3>";
        echo "</div>";

        echo "<p class=\"review-text\">" . $review['Content'] . "</p>";

        echo "</div>";

        echo "</div>";
    }
    echo "</div>";
}

?>
<!-- Write a Review -->
<?php
/* Don't need to show this section if user has already reviewed this park */
if (!isLoggedIn() || !hasAlreadyReviewed($parkID, $_SESSION['UserID'])) {
    ?>
    <h2>Write a Review</h2>

    <?php
    if (isLoggedIn()) {
        include "php/park/form.php";
    } else {
?>
      <!-- User isn't logged in so don't show the review form -->
      <p>You must be logged in to write a review.</p>
        <?php
    }
}

include "php/footer.php";



/*========== Functions ==========*/

// Returns an associate array of the parks info, or returns false if it doesn't exist
function getParkInfo($parkID)
{
    include "php/database.php";

    /* Get park info */
    try {
        $stmt = $pdo->prepare('SELECT * FROM items WHERE ParkID=:parkID');
        $stmt->bindValue(':parkID', $parkID);
        $stmt->execute();
        $count = $stmt->rowcount();
    } catch (PDOException $e) {
        echo $e->getMessage();
    }

    if ($count == 0) {
        return false;
    } else {
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
}

// Returns true if the user has already reviewed this park
function hasAlreadyReviewed($parkID, $userID)
{
    include "php/database.php";

    /* Check if user has submitted a review */
    try {
        $stmt = $pdo->prepare('SELECT * FROM reviews WHERE ParkID=:parkID AND UserID=:userID');
        $stmt->bindValue(':parkID', $parkID);
        $stmt->bindValue(':userID', $userID);
        $stmt->execute();
        $count = $stmt->rowcount();
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
    
    /* Has the user already reviewed this park? */
    if ($count >= 1) {
        return true;
    }

    return false;
}

// Returns an associate array of reviews for the park, or returns false if there aren't any reviews
function getReviews($parkID)
{
    include "php/database.php";

    /* Get reviews for the park */
    try {
        $stmt = $pdo->prepare('SELECT * FROM reviews ' .
        'INNER JOIN members ON reviews.UserID = members.UserID ' .
        'WHERE reviews.ParkID = :parkID');
        $stmt->bindValue(':parkID', $parkID);
        $stmt->execute();
        $count = $stmt->rowcount();
    } catch (PDOException $e) {
        echo $e->getMessage();
    }

    if ($count == 0) {
        return false;
    } else {
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
}

// Returns the number of reviews that the user has submitted to the database
function getUserReviewCountString($userID)
{
    include "php/database.php";

    /* Check if user has submitted a review */
    try {
        $stmt = $pdo->prepare('SELECT * FROM reviews WHERE UserID=:userID');
        $stmt->bindValue(':userID', $userID);
        $stmt->execute();
        $count = $stmt->rowcount();
    } catch (PDOException $e) {
        echo $e->getMessage();
        return;
    }

    if ($count == 1) {
        return $count . " review";
    } else {
        return $count . " reviews";
    }
}

// Submits the inputted review to the database.
function submitReview($parkID, $userID, $reviewTitle, $rating, $reviewText)
{
    include "php/database.php";

    if (hasAlreadyReviewed($parkID, $userID)) {
        return;
    }

    /* Insert new review into database */
    try {
        $stmt = $pdo->prepare('INSERT INTO reviews (ParkID, UserID, Rating, Title, Content) ' .
        'VALUES(:parkID, :userID, :rating, :title, :content)');
        $stmt->bindValue(':parkID', $parkID);
        $stmt->bindValue(':userID', $userID);
        $stmt->bindValue(':rating', $rating);
        $stmt->bindValue(':title', $reviewTitle);
        $stmt->bindValue(':content', $reviewText);
        $stmt->execute();
    } catch (PDOException $e) {
        echo $e->getMessage();
        return;
    }
}

function createMap($latitude, $longitude)
{
    ?>
    <div id="map"></div>
    <!-- Use Google Map API and PHP to generate map with park marked -->
    <script>
    function myMap() {      
        /* Properties */        
        var newLatlng = new google.maps.LatLng(<?=$latitude;?>,<?=$longitude;?>);
        var mapCanvas = document.getElementById("map");
        var mapOptions = {
          center: newLatlng, 
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          mapTypeControl: false
        };
        var map = new google.maps.Map(mapCanvas, mapOptions);

        /* Marker for the park */
        var marker = new google.maps.Marker({
            position: newLatlng,
            animation: google.maps.Animation.BOUNCE
        });
        
        marker.setMap(map);    
    }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDlkUHnzrn4VyZv2N-G2tJpZxXbJGJLDfg&callback=myMap"></script>
    <?php
}
