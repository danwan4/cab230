<?php
$PageTitle = "Search Results";



/*========== Content ==========*/

include "php/header.php";
?>

<h1>Search Results</h1>

<?php
processSearch();

include "php/footer.php";



/*========== Functions ==========*/

function processSearch()
{
    /* Searching for parks in a suburb */
    if (isset($_SESSION['suburb'])) {
        $suburb = $_SESSION['suburb'];
        suburbResult($suburb);
    /* Searching for a name */
    } elseif (isset($_SESSION['parkName'])) {
        $parkName = $_SESSION['parkName'];
        nameResult($parkName);
    /* Searching for parks with a rating of at least a certain number */
    } elseif (isset($_SESSION['rating'])) {
        $rating = $_SESSION['rating'];
        ratingResult($rating);
    /* Searching for parks within a certain distance */
    } elseif (isset($_SESSION['distance'])) {
        $distance = $_SESSION['distance'];
        distanceResult($distance);
    }
}

function suburbResult($suburb)
{
    include "php/database.php";

    try {
        $stmt = $pdo->prepare('SELECT items.ParkID, items.ParkCode, items.Name, items.Street, items.Suburb, items.Easting, items.Northing, items.Latitude, items.Longitude, AVG(reviews.Rating) ' .
        'FROM items ' .
        'LEFT OUTER JOIN reviews ON items.ParkID = reviews.ParkID ' .
        'WHERE Suburb=:suburb ' .
        'GROUP BY items.ParkID');
        $stmt->bindValue(':suburb', $suburb);
        $stmt->execute();
        $count = $stmt->rowcount();
        $result = $stmt->fetchAll();
    } catch (PDOException $e) {
        echo $e->getMessage();
        return false;
    }
    
    if ($count > 0) {
        echo "<p>Showing results for suburb \"$suburb\".</p>";
        EchoResults($result);
    } else {
        echo "<p>No results found for suburb \"$suburb\".</p>";
    }
}

function nameResult($parkName)
{
    include "php/database.php";
    
    try {
        $stmt = $pdo->prepare('SELECT items.ParkID, items.ParkCode, items.Name, items.Street, items.Suburb, items.Easting, items.Northing, items.Latitude, items.Longitude, AVG(reviews.Rating) ' .
        'FROM items ' .
        'LEFT OUTER JOIN reviews ON items.ParkID = reviews.ParkID ' .
        'WHERE Name LIKE :parkName ' .
        'GROUP BY items.ParkID');
        $stmt->bindValue(':parkName', "%$parkName%");
        $stmt->execute();
        $count = $stmt->rowcount();
        $result = $stmt->fetchAll();
    } catch (PDOException $e) {
        echo $e->getMessage();
        return false;
    }
    
    if ($count > 0) {
        echo "<p>Showing results for park name \"$parkName\".</p>";
        EchoResults($result);
    } else {
        echo "<p>No results found for park name \"$parkName\".</p>";
    }
}

function ratingResult($minimumRating)
{
    include "php/database.php";
    
    try {
        $stmt = $pdo->prepare('SELECT items.ParkID, items.ParkCode, items.Name, items.Street, items.Suburb, items.Easting, items.Northing, items.Latitude, items.Longitude, AVG(reviews.Rating) ' .
        'FROM items ' .
        'INNER JOIN reviews ON items.ParkID = reviews.ParkID ' .
        'GROUP BY items.ParkID ' .
        'HAVING AVG(reviews.rating) >= :minimumRating');
        $stmt->bindValue('minimumRating', $minimumRating);
        $stmt->execute();
        $count = $stmt->rowcount();
        $result = $stmt->fetchAll();
    } catch (PDOException $e) {
        echo $e->getMessage();
        return false;
    }

    if ($count > 0) {
        echo "<p>Showing parks with an average rating of <span class=\"review-rating\">" . str_repeat("★", $minimumRating) . "</span>or higher.</p>";
        EchoResults($result);
    } else {
        echo "<p>No results found for parks with an average rating of <span class=\"review-rating\">" . str_repeat("★", $minimumRating) . "</span>or higher.</p>";
    }
}

function distanceResult($distance)
{
    include "php/database.php";

    require_once('php/geoplugin.class.php');

    $geoplugin = new geoPlugin();
    $geoplugin->locate();
    
    $longitude = $geoplugin->longitude;
    $latitude = $geoplugin->latitude;
    $lat_difference = abs($distance/110.574);
    $long_difference = abs($distance/(111.320*cos($latitude)));
    
    $minLat = $latitude - $lat_difference;
    $maxLat = $latitude + $lat_difference;
    $minLong = $longitude - $long_difference;
    $maxLong = $longitude + $long_difference;
    
    try {
        $stmt = $pdo->prepare('SELECT items.ParkID, items.ParkCode, items.Name, items.Street, items.Suburb, items.Easting, items.Northing, items.Latitude, items.Longitude, AVG(reviews.Rating) ' .
        'FROM items ' .
        'LEFT OUTER JOIN reviews ON items.ParkID = reviews.ParkID ' .
        'WHERE items.Latitude BETWEEN :minLatitude AND :maxLatitude AND items.Longitude BETWEEN :minLongitude AND :maxLongitude ' .
        'GROUP BY items.ParkID');
        $stmt->bindValue('minLatitude', $minLat);
        $stmt->bindValue('maxLatitude', $maxLat);
        $stmt->bindValue('minLongitude', $minLong);
        $stmt->bindValue('maxLongitude', $maxLong);
        $stmt->execute();
        $count = $stmt->rowcount();
        $result = $stmt->fetchAll();
    } catch (PDOException $e) {
        echo $e->getMessage();
        return false;
    }

    if ($count > 0) {
        echo "<p>Showing parks within $distance kilometers of your location.</p>";
        EchoResults($result);
    } else {
        echo "<p>No results found for parks within $distance kilometers.</p>";
    }
}

function echoResults($parks)
{
    echo '<p>Double click on a marker on the map to go to the park page.</p>';
    CreateMap($parks);

    echo '<table class="resultstable">';

    /* Column Headers */
    echo '<tr>';
    echo '<th>Code</th>';
    echo '<th>Name</th>';
    echo '<th>Street</th>';
    echo '<th>Suburb</th>';
    echo '<th>Rating</th>';
    echo '</tr>';

    /* Rows */
    foreach ($parks as $park) {
        echo '<tr>';
        echo '<td>' . $park['ParkCode'] . '</td>';
        echo '<td><a href="park.php?park='.$park['ParkID'].'">'.$park['Name'].'</a></td>';
        echo '<td>' . $park['Street'] . '</td>';
        echo '<td>' . $park['Suburb'] . '</td>';
        if ($park['AVG(reviews.Rating)'] != null) {
            echo '<td>' . $park['AVG(reviews.Rating)'] . '</td>';
        } else {
            echo '<td>None</td>';
        }
        echo '</tr>';
    }

    echo '</table>';
}

function createMap($parks)
{
    ?>
    <div id="map"></div>
    <!-- Use Google Map API and PHP to generate map with all parks marked -->
    <script>
    function myMap() {     
        /* Properties */
        var mapCanvas = document.getElementById("map");
        var mapOptions = {
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          mapTypeControl: false
        };
        var map = new google.maps.Map(mapCanvas, mapOptions);
        var infowindow = new google.maps.InfoWindow();
        var bounds = new google.maps.LatLngBounds();
      
        /* Loop to add markers to the map */
        <?php
        foreach ($parks as $park) {
            ?>
            var newmarker = new google.maps.LatLng(<?=$park['Latitude'];?>,<?=$park['Longitude'];?>);
            bounds.extend(newmarker);
            var marker = new google.maps.Marker({
                position: newmarker,
                title: "<?=$park['Name'];?>",
                url: "park.php?park=<?=$park['ParkID'];?>"
            });
            marker.setMap(map); 
            google.maps.event.addListener(marker, 'click', (function(marker) {
                return function() {
                    infowindow.setContent("<?=$park['Name'];?>");
                    infowindow.open(map, marker);
                }
            })(marker));
            google.maps.event.addListener(marker, 'dblclick', (function(marker) {
                return function() {
                   window.location.href = this.url;
                }
            })(marker));
            
            <?php
        }?>

        /* Centre and zoom map accordingly to bounds */
        map.fitBounds(bounds);
        map.panToBounds(bounds);
    }
    </script>
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDlkUHnzrn4VyZv2N-G2tJpZxXbJGJLDfg&callback=myMap"></script>
    <?php
}
