<?php

/*========== Common PHP Functions ==========*/

// Filter an input string, removing unnecessary extra characters, slashes,
// and converts any special HTML characters to prevent XSS attacks.
function filterInput($input)
{
    $input = trim($input);
    $input = stripslashes($input);
    $input = htmlspecialchars($input);
    return $input;
}

// Returns true if user is logged in
function isLoggedIn()
{
    return isset($_SESSION['UserID']);
}