<?php

// Username
if (empty($username)) {
    $usernameError = "This is a required field.";
    $validInputs = false;
}

// Password
if (empty($password)) {
    $passwordError = "This is a required field.";
    $validInputs = false;
}
