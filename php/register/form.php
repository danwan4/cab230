<p>Please enter your details in the fields below.</p>

<form name="register" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">

  <div class="formInputContainer">

    <!-- First Column -->
    <div>
      <!-- Username -->
      <div class="formInput">
        <div class="formInputTitle">Username</div>
        <input type="text" class="formInputField" name="username" onkeypress="hideElement('usernameInvalid');" value=<?=$username?>>
        <div id="usernameInvalid" class="formError"><?=$usernameError?></div>
      </div>
      <!-- Email -->
      <div class="formInput">
        <div class="formInputTitle">Email</div>
        <input type="text" class="formInputField" name="email" onkeypress="hideElement('emailInvalid');" value=<?=$email?>>
        <div id="emailInvalid" class="formError"><?=$emailError?></div>
      </div>
      <!-- Password -->
      <div class="formInput">
        <div class="formInputTitle">Password</div>
        <input type="password" class="formInputField" name="password" onkeypress="hideElement('passwordInvalid');" value=<?=$password?>>
        <div id="passwordInvalid" class="formError"><?=$passwordError?></div>
      </div>
      <!-- Confirm Password -->
      <div class="formInput">
        <div class="formInputTitle">Confirm Password</div>
        <input type="password" class="formInputField" name="passwordRepeated" onkeypress="hideElement('passwordRepeatedInvalid');" value=<?=$passwordRepeated?>>
        <div id="passwordRepeatedInvalid" class="formError"><?=$passwordRepeatedError?></div>
      </div>
    </div>

    <!-- Second Column -->
    <div>
      <!-- Email -->
      <div class="formInput">
        <div class="formInputTitle">First Name</div>
        <input type="text" class="formInputField" name="firstName" onkeypress="hideElement('firstNameInvalid');" value=<?=$firstName?>>
        <div id="firstNameInvalid" class="formError"><?=$firstNameError?></div>
      </div>
      <!-- Email -->
      <div class="formInput">
        <div class="formInputTitle">Last Name</div>
        <input type="text" class="formInputField" name="lastName" onkeypress="hideElement('lastNameInvalid');" value=<?=$email?>>
        <div id="lastNameInvalid" class="formError"><?=$lastNameError?></div>
      </div>
      <!-- Date of Birth -->
      <div class="formInput">
        <div class="formInputTitle">Date of Birth</div>
        <input type="text" class="formInputField" name="dateOfBirth" placeholder="DD/MM/YYYY" onkeypress="hideElement('dateOfBirthInvalid');" value=<?=$dateOfBirth?>>
        <div id="dateOfBirthInvalid" class="formError"><?=$dateOfBirthError?></div>
      </div>
      <!-- Post Code -->
      <div class="formInput">
        <div class="formInputTitle">Post Code</div>
        <input type="text" class="formInputField" name="postCode" onkeypress="hideElement('postCodeInvalid');" value=<?=$postCode?>>
        <div id="postCodeInvalid" class="formError"><?=$postCodeError?></div>
      </div>
    </div>

  </div>

  <!-- Submit Button -->
  <input type="submit" class="formSubmitButton" value="Register" onclick="return validateRegisterForm();">
  <div id="submitError" class="formError"><?=$submitError?></div>
</form>
