<?php

// Username
if (empty($username)) {
    $usernameError = "This is a required field.";
    $validInputs = false;
} elseif (!preg_match("/^[a-zA-Z0-9]+$/", $username)) {
    $usernameError = "Your username contains unsupported characters.";
    $validInputs = false;
} elseif (!checkUniqueUsername($username)) {
    $usernameError = "That username is already being used.";
    $validInputs = false;
}

// Email
if (empty($email)) {
    $emailError = "This is a required field.";
    $validInputs = false;
} elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $emailError = "Please enter a valid e-mail address.";
    $validInputs = false;
} elseif (!checkUniqueEmail($email)) {
    $emailError = "That email is already associated with another account.";
    $validInputs = false;
}

// Password
if (empty($password)) {
    $passwordError = "This is a required field.";
    $validInputs = false;
} elseif (strlen($password) < 6) {
    $passwordError = "Your password must be at least 6 characters long.";
    $validInputs = false;
}

// Password repeated
if (empty($passwordRepeated)) {
    $passwordRepeatedError = "This is a required field.";
    $validInputs = false;
} elseif ($password != $passwordRepeated) {
    $passwordRepeatedError = "The passwords you entered were different.";
    $validInputs = false;
}

// First name
if (empty($firstName)) {
    $firstNameError = "This is a required field.";
    $validInputs = false;
} elseif (!preg_match("/^[a-zA-Z ]+$/", $firstName)) {
    $firstNameError = "Your first name contains unsupported characters.";
    $validInputs = false;
}

// Last name
if (empty($lastName)) {
    $lastNameError = "This is a required field.";
    $validInputs = false;
} elseif (!preg_match("/^[a-zA-Z ]+$/", $lastName)) {
    $lastNameError = "Your last name contains unsupported characters.";
    $validInputs = false;
}

// Date of birth
$dateOfBirthRegExp = "/(^(((0[1-9]|1[0-9]|2[0-8])[\/](0[1-9]|1[012]))|((29|30|31)[\/](0[13578]|1[02]))|((29|30)[\/](0[4,6,9]|11)))[\/](19|[2-9][0-9])\d\d$)|(^29[\/]02[\/](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/";
if (empty($dateOfBirth)) {
    $dateOfBirthError = "This is a required field.";
    $validInputs = false;
} elseif (!preg_match($dateOfBirthRegExp, $dateOfBirth)) {
    $dateOfBirthError = "Please enter a valid date (DD/MM/YYYY).";
    $validInputs = false;
}

// Post code
if (empty($postCode)) {
    $postCodeError = "This is a required field.";
    $validInputs = false;
} elseif (!preg_match("/^[0-9]{4}$/", $postCode)) {
    $postCodeError = "Please enter a valid post code.";
    $validInputs = false;
}
