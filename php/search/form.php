<!-- Park Name Search Form -->
<h2>Search by Name</h2>
<form name="nameSearch" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
    <div class="formInput">
        <input type="text" class="formInputField" name="parkName" onkeypress="hideElement('nameInvalid');" value=<?=$name?>>
    </div>
    <input type="submit" class="formSubmitButton" value="Search" onclick="return validateNameForm();">
    <div id="nameInvalid" class="formError"><?=$nameError?></div>
</form>

<!-- Suburb Search Form -->
<h2>Search by Suburb</h2>
<form name="suburbSearch" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
    <div class="formInput">
        <select class="formInputField" name="suburb" onchange="hideElement('suburbInvalid');">
            <option value="" selected disabled hidden>Select a suburb...</option>';
            <?php
            foreach ($result as $park) {
                echo '<option value="', $park['Suburb'],'">', $park['Suburb'],'</option>';
            }?>
        </select>
    </div>
    <input type="submit" class="formSubmitButton" value="Search" onclick="return validateSuburbForm();">
    <div id="suburbInvalid" class="formError"><?=$suburbError?></div>
</form>

<!-- Distance Search Form -->
<h2>Search by Distance</h2>
<form name="distanceSearch" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
    <div class="formInput">
        <input type="text" class="formInputField" name="distance" onkeypress="hideElement('distanceInvalid');" placeholder="Distance in kilometers.." value=<?=$distance?>>
    </div>
    <input type="submit" class="formSubmitButton" value="Search" onclick="return validateDistanceForm();">
    <div id="distanceInvalid" class="formError"><?=$distanceError?></div>
</form>

<!-- Minimum Rating Search Form -->
<h2>Search by Minimum Rating</h2>
<form name="ratingSearch" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
    <div class="formInput">
        <select class="formInputField" name="rating" onchange="hideElement('ratingInvalid');">
            <option value="" selected disabled hidden>Select a rating...</option>
            <?php
            for ($i=1; $i <= 5; $i++) {
                echo "<option value=\"$i\"";
                /* Select the rating if it has been POSTed but failed server validation */
                if ($rating == $i) {
                    echo " selected";
                }
                echo " class=\"review-rating\">" . str_repeat("★", $i) . "</option>";
            }?>
        </select>             
    </div>
    <input type="submit" class="formSubmitButton" value="Search" onclick="return validateRatingForm();">
    <div id="ratingInvalid" class="formError"><?=$ratingError?></div>
</form>
