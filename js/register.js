// Validates the register form inputs, returning true if valid
function validateRegisterForm() {
	var validUsername = checkUsername();
	var validEmail = checkEmail();
	var validPassword = checkPassword();
	var validPasswordRepeated = checkPasswordRepeated();
	var validFirstName = checkFirstName();
	var validLastName = checkLastName();
	var validDateOfBirth = checkDateOfBirth();
	var validPostCode = checkPostCode();

	if (validUsername && validEmail && validPassword && validPasswordRepeated
		&& validFirstName && validLastName && validDateOfBirth && validPostCode) {
		return true;
	} else {
		return false;
	}
}

// Hides an element of a given id
function hideElement(id) {
	document.getElementById(id).style.visibility = "hidden";
}

// Checks if username input is valid
function checkUsername() {
	var field = document.forms["register"]["username"].value;
	var re = /^[a-zA-Z0-9_]+$/;

	// Check if input is empty
	if (field === "") {
		document.getElementById("usernameInvalid").innerHTML = "This is a required field.";
		document.getElementById("usernameInvalid").style.visibility = "visible";
		return false;
	}
	// Check if username has supported characters as specified by regular expression
	else if (!re.test(field)) {
		document.getElementById("usernameInvalid").innerHTML = "Your username contains unsupported characters.";
		document.getElementById("usernameInvalid").style.visibility = "visible";
		return false;
	}
	return true;
}

// Checks if email input is valid
function checkEmail() {
	var field = document.forms["register"]["email"].value;
	var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

	// Check if input is empty
	if (field === "") {
		document.getElementById("emailInvalid").innerHTML = "This is a required field.";
		document.getElementById("emailInvalid").style.visibility = "visible";
		return false;
	}
	// Check if email is correct format
	else if (!re.test(field)) {
		document.getElementById("emailInvalid").innerHTML = "Please enter a valid e-mail address.";
		document.getElementById("emailInvalid").style.visibility = "visible";
		return false;
	}
	return true;
}

// Checks if password input is valid
function checkPassword() {
	var field = document.forms["register"]["password"].value;

	// Check if input is empty
	if (field === "") {
		document.getElementById("passwordInvalid").innerHTML = "This is a required field.";
		document.getElementById("passwordInvalid").style.visibility = "visible";
		return false;
	}
	// Check if password is long enough
	else if (field.length < 6) {
		document.getElementById("passwordInvalid").innerHTML = "Your password must be at least 6 characters long.";
		document.getElementById("passwordInvalid").style.visibility = "visible";
		return false;
	}
	return true;
}

// Checks if repeated password input is valid
function checkPasswordRepeated() {
	var password = document.forms["register"]["password"].value;
	var password_repeated = document.forms["register"]["passwordRepeated"].value;

	// Check if input is empty
	if (password_repeated === "") {
		document.getElementById("passwordRepeatedInvalid").innerHTML = "This is a required field.";
		document.getElementById("passwordRepeatedInvalid").style.visibility = "visible";
	}
	// Check if passwords match
	if (password != password_repeated) {
		document.getElementById("passwordRepeatedInvalid").innerHTML = "The passwords you entered were different.";
		document.getElementById("passwordRepeatedInvalid").style.visibility = "visible";
	}
	return true;
}

// Checks if first name input is valid
function checkFirstName() {
	var field = document.forms["register"]["firstName"].value;
	var re = /^[a-zA-Z ]+$/;

	// Check if input is empty
	if (field === "") {
		document.getElementById("firstNameInvalid").innerHTML = "This is a required field.";
		document.getElementById("firstNameInvalid").style.visibility = "visible";
		return false;
	}
	// Check if name has normal characters as specified by regular expression
	else if (!re.test(field)) {
		document.getElementById("firstNameInvalid").innerHTML = "Your first name contains unsupported characters.";
		document.getElementById("firstNameInvalid").style.visibility = "visible";
		return false;
	}
	return true;
}

// Checks if last name input is valid
function checkLastName() {
	var field = document.forms["register"]["lastName"].value;
	var re = /^[a-zA-Z ]+$/;

	// Check if input is empty
	if (field === "") {
		document.getElementById("lastNameInvalid").innerHTML = "This is a required field.";
		document.getElementById("lastNameInvalid").style.visibility = "visible";
		return false;
	}
	// Check if name has normal characters as specified by regular expression
	else if (!re.test(field)) {
		document.getElementById("lastNameInvalid").innerHTML = "Your last name contains unsupported characters.";
		document.getElementById("lastNameInvalid").style.visibility = "visible";
		return false;
	}
	return true;
}

// Checks if date of birth input is valid
function checkDateOfBirth() {
	var field = document.forms["register"]["dateOfBirth"].value;
	var re = /(^(((0[1-9]|1[0-9]|2[0-8])[\/](0[1-9]|1[012]))|((29|30|31)[\/](0[13578]|1[02]))|((29|30)[\/](0[4,6,9]|11)))[\/](19|[2-9][0-9])\d\d$)|(^29[\/]02[\/](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/;

	// Check if input is empty
	if (field === "") {
		document.getElementById("dateOfBirthInvalid").innerHTML = "This is a required field.";
		document.getElementById("dateOfBirthInvalid").style.visibility = "visible";
		return false;
	}
	// Check if date is correct format as specified by regular expression (DD/MM/YY)
	else if (!re.test(field)) {
		document.getElementById("dateOfBirthInvalid").innerHTML = "Please enter a valid date (DD/MM/YYYY).";
		document.getElementById("dateOfBirthInvalid").style.visibility = "visible";
		return false;
	}
	return true;
}

// Checks if post code input is valid
function checkPostCode() {
	var field = document.forms["register"]["postCode"].value;
	var re = /^[0-9]{4}$/;

	// Check if input is empty
	if (field === "") {
		document.getElementById("postCodeInvalid").innerHTML = "This is a required field.";
		document.getElementById("postCodeInvalid").style.visibility = "visible";
		return false;
	}
	// Check if postcode is correct format as specified by regular expression
	else if (!re.test(field)) {
		document.getElementById("postCodeInvalid").innerHTML = "Please enter a valid post code.";
		document.getElementById("postCodeInvalid").style.visibility = "visible";
		return false;
	}
	return true;
}