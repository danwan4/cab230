<?php
$PageTitle = "Search";

$name = "";
$nameError = "";
$suburb = "";
$suburbError = "";
$distance = "";
$distanceError = "";
$rating = "";
$ratingError = "";

include "php/database.php";

try {
    $result = $pdo->query('SELECT DISTINCT Suburb FROM items ORDER BY Suburb ASC');
} catch (PDOException $e) {
    echo $e->getMessage();
}



include_once "php/functions.php";



/*========== Content ==========*/

include "php/header.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    require "php/search/process.php";
}
?>

<script type="text/javascript" src="js/search.js"></script>

<h1>Search for Park</h1>
 
<?php

include "php/search/form.php";

include "php/footer.php";
