<?php session_start(); ?>
<!DOCTYPE html>

<html>

<head>
  <!-- Meta -->
  <meta charset="UTF-8">
  <meta name="description" content="Brisbane Parks search and reviews">
  <meta name="author" content="Jason Chang, Daniel Wan, Luke Yeh">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Style Sheet -->
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link href="https://fonts.googleapis.com/css?family=Open Sans" rel="stylesheet">
  
  <!-- Set title of page based on value of $PageTitle -->
  <title>
    <?= isset($PageTitle) ? "BrisParks - " . $PageTitle : "Default Title"?>
  </title>
</head>

<body>
  <div id="header-background"></div>

  <div id="wrapper">
    <header>
      <!-- Logo on left side of header/navigation bar -->
      <div class="logo">
        <a href="index.php">BrisParks</a>
      </div>
      <!-- Navigation links on right side of header/navigation bar -->
      <nav>
        <!-- If logged in, so show Logout button and not Register or Login buttons -->
        <?php if (isset($_SESSION['UserID'])) { ?>
        <ul>
          <li><a href="search.php">Search</a></li>
          <li><a href="logout.php">Logout</a></li>
        </ul>
        <!-- Not logged in, so show Register and Login button -->
        <?php } else { ?>
        <ul>
          <li><a href="search.php">Search</a></li>
          <li><a href="register.php">Register</a></li>
          <li><a href="login.php">Login</a></li>
        </ul>
        <?php } ?>
      </nav>
    </header>

    <!-- Open up the the content box which is below the header/navigation bar -->
    <div id="main-wrapper">
      <main>
